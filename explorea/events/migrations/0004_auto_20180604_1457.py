# Generated by Django 2.0.5 on 2018-06-04 14:57

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0003_event_host'),
    ]

    operations = [
        migrations.RenameField(
            model_name='eventrun',
            old_name='happens',
            new_name='date'
        ),
        migrations.AddField(
            model_name='eventrun',
            name='time',
            field=models.TimeField(default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
