from django.urls import path
from django.conf.urls import url
from django.contrib.auth import views as auth_views

from . import views

app_name = 'accounts'

urlpatterns = [
    path('login/', auth_views.login, {'template_name': 'accounts/login.html'}, name='login'),
    path('logout/', auth_views.logout, {'next_page': '/'}, name='logout'),
    
    path('register/', views.RegisterView.as_view(), name='register'),
    path('profile/', views.ProfileView.as_view(), name='profile'),
    path('profile/edit/', views.EditProfileView.as_view(), name='edit_profile'),
    path('change-password/', views.ChangePasswordView.as_view(), name='change_password'),

    path('reset-password/', auth_views.password_reset, 
        {'template_name': 'accounts/reset_password.html', 
        'post_reset_redirect':'accounts:password_reset_done',
        'email_template_name':'accounts/reset_password_email.html'}, name='reset_password'),
    path('reset-password/done/', auth_views.password_reset_done,  name='password_reset_done'),
    url(r'^reset-password/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', auth_views.password_reset_confirm, 
        {'post_reset_redirect':'accounts:password_reset_complete'}, name='password_reset_confirm'),
    path('reset-password/complete/', auth_views.password_reset_complete, name='password_reset_complete'),

    path('hosts/', views.HostListView.as_view(), name='host_list'),
    path('become-host/', views.BecomHostView.as_view(), name='become_host'),
    url(r'^become-host/confirm/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>.+)/$', views.ActivateHost.as_view(), 
        name='activate_host'),
    path('host/profile/<username>/', views.HostProfileView.as_view(), name='host_profile'),

    path('ajax/validate_exists/', views.ValidateRegisterFormView.as_view(), name='validate_register_form'),
]